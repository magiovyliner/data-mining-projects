import json
import scrapy
from scrapy.loader import ItemLoader
from project_1.items import Job
from scrapy.mail import MailSender
from HTMLParser import HTMLParser
import unicodedata

class JobsSpider(scrapy.Spider):
    name = "jobs"
    content = ""
    base_url = "https://www.freelancer.ph/ajax/table/project_contest_datatable.php?"
    main_url = "https://www.freelancer.ph"
    params = {
        "type" : "false",
        "budget_min" : "false",
        "budget_max" : "false",
        "contest_budget_min" : "false",
        "contest_budget_max" : "false",
        "hourlyrate_min" : "false",
        "hourlyrate_max" : "false",
        "hourlyProjectDuration" : "false",
        "skills_chosen" : "false",
        "languages" : "false",
        "status" : "open",
        "vicinity" : "false",
        "countries" : "false",
        "lat" : "false",
        "lon" : "false",
        "iDisplayStart" : 0,
        "iDisplayLength" : 20,
        "iSortingCols" : 1,
        "iSortCol_0" : 6,
        "sSortDir_0" : "desc",
        "format_version" : 2
    }
    h = HTMLParser()

    def start_requests(self):
        url = self.build_param_string()
        tag = getattr(self, 'tag', None)
        if tag is not None:
            url = url + "tag=" + tag
        yield scrapy.Request(url, callback=self.parse)
    
    def build_param_string(self):
        param_string = self.base_url

        for key, value in self.params.items():
            param_string += key + "=" + str(value) + "&"
        
        return param_string

    def parse(self, response):
        jsonresponse = json.loads(response.body)
       
        for data in jsonresponse['aaData']:
            l = ItemLoader(item=Job())
            l.add_value('project_name', data['project_name'])
            l.add_value('project_desc', data['project_desc'])
            l.add_value('url', data['seo_url'])
            l.add_value('bid_avg', data['bid_avg'])
            l.add_value('bid_count', data['bid_count'])
            l.add_value('time_left', data['time_left'])
            self.construct_email_content(data)
            
            yield l.load_item()
        
        mailer = Mailer("Web Scraping Jobs for Today", self.content)
        mailer.send_mail()

    def construct_email_content(self, item):
        self.content += "=========================<br>"
        self.content += "Project Name: %s <br>" % item['project_name']
        self.content += "Project Desc: %s <br>" % item['project_desc']
        self.content += "URL: %s%s <br>" % (self.main_url, item['seo_url'])
        self.content += "Bid Avg: " + ('%s' % item['bid_avg']).encode('utf-8','ignore') + "<br>"
        self.content += "Bid Count: %s <br>" % item['bid_count']
        self.content += "Time Left: %s <br>" % item['time_left']
        
        return

class Mailer(object):

    settings = {
        "smtphost" : "smtp.gmail.com",
        "mailfrom" : "Project1",
        "smtpuser" : "test@gmail.com",
        "smtppass" : "testpass",
        "smtpport" : 465,
        "smtpssl" : True
    }

    mailer_send = {
        "to" : ["palf16@gmail.com"],
        "mimetype" : "text/html"
    }

    def __init__(self, subject, content):
        self.subject = subject
        self.content = content
        return

    def send_mail(self):
        mailer = MailSender(
            smtphost=self.settings["smtphost"],
            mailfrom=self.settings["mailfrom"],
            smtpuser=self.settings["smtpuser"],
            smtppass=self.settings["smtppass"],
            smtpport=self.settings["smtpport"],
            smtptls=not self.settings["smtpssl"],
            smtpssl=self.settings["smtpssl"]
        )

        mailer.send(
            to=self.mailer_send["to"],
            subject=self.subject,
            body=self.content,
            mimetype=self.mailer_send['mimetype']
        )

        return
