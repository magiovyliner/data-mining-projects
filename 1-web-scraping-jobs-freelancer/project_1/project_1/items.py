# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Job(scrapy.Item):
    project_name = scrapy.Field()
    project_desc = scrapy.Field()
    url = scrapy.Field()
    bid_avg = scrapy.Field()
    bid_count = scrapy.Field()
    time_left = scrapy.Field()
