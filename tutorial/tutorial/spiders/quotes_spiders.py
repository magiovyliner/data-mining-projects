import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        'http://quotes.toscrape.com/page/1/'
    ]

    def parse(self, response):
        for quote in response.xpath(".//div[@class='quote']"):
            yield {
                'tags' : quote.xpath(".//div[@class='tags']//a[@class='tag']/text()").extract(),
                'author' : quote.xpath(".//small[@class='author']/text()").extract_first(),
                'text' : quote.xpath(".//span[@class='text']/text()").extract_first()
            }
        
        href = response.xpath(".//li[@class='next']/a/@href").extract_first()
        yield response.follow(href, callback=self.parse)

class AuthorSpider(scrapy.Spider):
    name = "author"
    start_urls = [
        'http://quotes.toscrape.com'
    ]

    def parse(self, response):
        for author in response.xpath(".//div[@class='quote']//a[text()='(about)']"):
            yield response.follow(author, callback=self.parse_author)
        
        href = response.xpath(".//li[@class='next']/a/@href").extract_first()
        yield response.follow(href, callback=self.parse)
    
    def parse_author(self,response):
        author_details = response.xpath(".//div[@class='author-details']")
        author_name = author_details.xpath(".//h3[@class='author-title']/text()").extract_first().strip()
        author_born_date = author_details.xpath(".//span[@class='author-born-date']/text()").extract_first().strip()
        author_born_location = author_details.xpath(".//span[@class='author-born-location']/text()").extract_first().strip()

        yield {
            "author_name" : author_name,
            "author_born_date" : author_born_date,
            "author_born_location" : author_born_location
        }

class TagSpider(scrapy.Spider):
    name = "tag"
    
    def start_requests(self):
        url = 'http://quotes.toscrape.com/'
        tag = getattr(self, 'tag', None)
        if tag is not None:
            url = url + "tag/" + tag
        yield scrapy.Request(url, callback=self.parse)
    
    def parse(self, response):
        for quote in response.xpath(".//div[@class='quote']"):
            yield {
                'tags' : quote.xpath(".//div[@class='tags']//a[@class='tag']/text()").extract(),
                'author' : quote.xpath(".//small[@class='author']/text()").extract_first(),
                'text' : quote.xpath(".//span[@class='text']/text()").extract_first()
            }
        
        href = response.xpath(".//li[@class='next']/a/@href").extract_first()
        yield response.follow(href, callback=self.parse)